import tweetsData from "../data/tweetsdata.json";
import alasql from 'alasql';

let data = tweetsData.Tweets;

for (let index = 0; index < data.length; index++) {
    data[index].created_at = data[index].created_at.substr(0, 10);
}

// eslint-disable-next-line no-multi-str
let graphFriendsData = alasql('SELECT created_at, MAX(user_friends_count)  AS Nb_friends \
                        FROM ? \
                        GROUP BY created_at', [data]);

export default graphFriendsData;