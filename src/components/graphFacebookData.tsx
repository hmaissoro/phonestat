import facebookData from "../data/facebookdata.json";
import alasql from 'alasql';

let data = facebookData.posts.data;

for (let index = 0; index < data.length; index++) {
    data[index].created_time = data[index].created_time.substr(0, 10);
}

// eslint-disable-next-line no-multi-str
let graphPostsData = alasql('SELECT created_time, COUNT(id) AS Nb_posts \
                        FROM ? \
                        GROUP BY created_time', [data]);

export default graphPostsData;