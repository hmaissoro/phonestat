import React from "react";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonIcon,
} from "@ionic/react";

import "./Twitter.css";

import graphTweetsData from "../components/graphTweetsData";
import graphFollowersData from "../components/graphFollowersData";
import graphFriendsData from "../components/graphFriendsData";

import GraphTweets from "../components/GraphTweets";
import GraphFollowers from "../components/GraphFollowers";
import GraphFriends from "../components/GraphFriends";
import { statsChart } from "ionicons/icons";

console.log(graphTweetsData);
let date = [];
let nb_tweets = [];
let nb_followers = [];
let nb_friends = [];

for (let i = 0; i < graphTweetsData.length; i++) {
  date.push(graphTweetsData[i].created_at);
  nb_tweets.push(graphTweetsData[i].Nb_tweets);
  nb_followers.push(graphFollowersData[i].Nb_followers);
  nb_friends.push(graphFriendsData[i].Nb_friends);
}
// props pour la création du digramme en bar (nombre de tweets)
let option1 = {
  chart: {
    id: "basic-bar"
  },
  title: {
    text: "Nombre de tweets par jour."
  },
  plotOptions: {
    bar: {
      horizontal: true,
    }
  },
  dataLabels: {
    enabled: false
  },
  xaxis: {
    categories: date
  }
};

let series1 = [
  {
    name: "Nombre de tweets",
    data: nb_tweets
  }
];

// props pour la création des courbes nombre de followers/friends
// eslint-disable-next-line
let option2 = {
  chart: {
    id: "basic-bar"
  },
  title: {
    text: "Evolution du nombre d'abonnés."
  },
  xaxis: {
    categories: date
  }
};
// eslint-disable-next-line
let series2 = [
  {
    name: "Nombre d'abonnés",
    data: nb_followers
  }
];
// eslint-disable-next-line
let option3 = {
  chart: {
    id: "basic-bar"
  },
  title: {
    text: "Evolution du nombre d'amis."
  },
  xaxis: {
    categories: date
  }
};
let series3 = [
  {
    name: "Nombre d'amis",
    data: nb_friends
  }
];

const Twitter: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar class="header">
          <IonTitle class="IonTitle">
            <IonIcon icon={statsChart} class="IonIcon" />  Twitter
            </IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent class="IonContent">
        <GraphTweets options={option1} series={series1} />
        <GraphFollowers options={option2} series={series2} />
        <GraphFriends options={option3} series={series3} />
      </IonContent>
    </IonPage>
  );
};

export default Twitter;
