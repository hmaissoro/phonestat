import { IonContent, IonItem, IonThumbnail, IonLabel, IonHeader, IonPage, IonTitle, IonToolbar, IonButton, IonIcon } from '@ionic/react';
import React, { Component } from 'react';
import { Plugins } from '@capacitor/core';
import { statsChart } from 'ionicons/icons';
import "./Facebook.css";
import GraphPosts from '../components/GraphFacebook';
import graphFacebookData from '../components/graphFacebookData';

let time = []
let nb_posts = [];
for (let i = 0; i < graphFacebookData.length; i++) {
  time.push(graphFacebookData[i].created_time);
  nb_posts.push(graphFacebookData[i].Nb_posts);
}

// props pour la création des courbes nombre de

let option4 = {
  chart: {
    id: "basic-bar"
  },
  plotOptions: {
    bar: {
      horizontal: true,
    }
  },
  title: {
    text: "Nombre de posts par date"
  },
  dataLabels: {
    enabled: false
  },
  xaxis: {
    categories: time
  }
};

let series4 = [
  {
    name: "Nombre de posts",
    data: nb_posts
  }
];


const INITIAL_STATE = {
  user: {}
};

class Home extends Component {
  state: any = {};
  props: any = {};
  constructor(props: any) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }
  componentDidMount() {
    this.getUserInfo();
  }
  async getUserInfo() {
    const response = await fetch(`https://graph.facebook.com/${this.props.location.state.userId}?fields=id,name,gender,link,picture,posts{created_time}&type=large&access_token=${this.props.location.state.token}`);
    const myJson = await response.json();
    this.setState({
      user: myJson
    })
  }
  //--------------
  async signOut(): Promise<void> {
    const { history } = this.props;
    await Plugins.FacebookLogin.logout();
    history.goBack();
  }

  render() {
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar class="header">
            <IonTitle class="IonTitle">
              <IonIcon icon={statsChart} class="IonIcon" />  Facebook
            </IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent className="ion-padding">

          {this.state.user.name &&
            <IonItem>
              <IonThumbnail slot="start">
                <img src={this.state.user.picture.data.url} />
              </IonThumbnail>
              <IonLabel>
                <h3>{this.state.user.name}</h3>
              </IonLabel>
            </IonItem>
          }

          <GraphPosts options={option4} series={series4} />

          <IonButton className="login-button" onClick={() => this.signOut()} expand="full" fill="solid" color="danger">
            Déconnexion
        </IonButton>
        </IonContent>
      </IonPage>
    )
  }
}

export default Home;