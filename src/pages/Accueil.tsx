import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Accueil.css';

const Tab1: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar class="header">
          <IonTitle class="IonTitle">Bienvenu dans PhoneStat</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <div className="Msg">
          Cette application permet de suivre ses activités sur Twitter et Facebook.
          <br>
          </br>
          <br></br>
                Elle est développée par : <strong> MAISSORO Hassan</strong> et <strong> PENE Daouda</strong>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
