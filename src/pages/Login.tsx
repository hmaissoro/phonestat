import { IonContent, IonText, IonRow, IonCol, IonHeader, IonPage, IonTitle, IonToolbar, IonButton, IonImg, IonIcon } from '@ionic/react';
import React, { Component } from 'react';
import './Login.css';
import { Plugins } from '@capacitor/core';
import { statsChart, logoFacebook } from 'ionicons/icons';

const INITIAL_STATE = {
};

class Login extends Component {
    state: any = {};
    props: any = {};
    constructor(props: any) {
        super(props);
        this.state = { ...INITIAL_STATE };
    }

    async getCurrentState(): Promise<boolean> {
        const result = await Plugins.FacebookLogin.getCurrentAccessToken();
        try {
            return result && result.accessToken;
        } catch (e) {
            return false;
        }
    }

    async signIn(): Promise<void> {
        const { history } = this.props;
        const FACEBOOK_PERMISSIONS = ['public_profile', 'email'];
        const result = await Plugins.FacebookLogin.login({ permissions: FACEBOOK_PERMISSIONS });
        if (result && result.accessToken) {
            history.push({
                pathname: '/facebook',
                state: { token: result.accessToken.token, userId: result.accessToken.userId }
            });
        }
    }

    render() {
        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar class="header">
                        <IonTitle class="IonTitle">
                            <IonIcon icon={statsChart} class="IonIcon" />  Facebook
                        </IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent className="ion-padding">
                    <IonRow>
                        <IonCol className="text-center">
                            <IonIcon icon={logoFacebook} class="IonIconFacebook" />
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol className="text-center">
                            <IonText className="title">
                                Connexion à Facebook
                             </IonText>
                        </IonCol>
                    </IonRow>
                    <IonButton className="login-button" onClick={() => this.signIn()} expand="full" fill="solid" color="primary">
                        Se connecter
          </IonButton>
                </IonContent>
            </IonPage>
        )
    }
}
export default Login;